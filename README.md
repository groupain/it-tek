# IT Tek

## Le projet :

Usages limité de la téléphonie en situation de mobilité

## Les objectifs :

- Solution sécurisée entre téléphone mobile et IPBX (installation téléphonique fixe) du client
- Bénéfices et limites de la solution proposée
- Valeur ajoutée par rapport à l’existant (côté technique)

## Les pré-requis techniques :

- IPBX basée sur Asterisk (Issabel), téléphones mobiles de type Android / iPhone
- Le téléphone mobile fonctionnera en 4G (mais peut être simulé sur un WIFI Public)



Solutions à l'étude :
*  Linphone
*  GSwave
*  Zoiper
*  MicroSIP