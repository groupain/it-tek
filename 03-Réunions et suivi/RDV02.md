## Compte rendu de la réunion du 02 juillet 2019 ##

Présents : IT-Tek, Rennes, Nantes

Absents :




### Compte rendu ####

- Objectif :
L'objctif principal de ce projet est de permettre aux commerciaux qui sont en déplacement, ainsi qu'aux collaborateurs en télétravail, de béneficier des
mêmes accès téléphoniques que lorsqu'ils sont en entreprise. C'est à dire leurs numéro de téléphone fixe, la messagerie vocale de la ligne fixe,
des horaires d'appels ainsi qu'une redirection des appels lorsqu'ils sont en congés où absent.

- But :
Le but principal à atteindre avec ce projet est la convergence de la ligne fixe et de la ligne mobile de manière sécurisé.

- Rendu :
Devront être rendu à la fin du pojet une explication de la solution choisie sans omettre le cheminement qui nous a mené à cette solution, c'est à dire les
différentes solutions qui ont été envisagées. Il faudra aussi réaliser des test de la solution proposé afin de prouver son fonctionnement. Une documentation pourrat
être mise en place reprennant les différents elements mis en place ainsi qu'une documentation sur l'utilisation.

La solution proposé doit être fonctionnel pour les IPBX Asterisk
simuler la connexion internet entre deux ip publiques (celle du telephone et celle de l'ipbx)

- Exitant :
Les téléphones IP utilisés en entreprise sont de la marque Grandstream, GSWave est quand à lui le softphone utilisé dans cette entreprise.

- Sécurité :
En terme de sécurité, seul les téléphones portables de collaborateurs pourront accéder à l'IPBX de l'externe.

- Financier :
Seul des solutions libre pourront être adopté pour ce projet, les seuls cout de ce projet seront le temps passé à sa réalisation.




Rédacteur : Antoine COUTARD
